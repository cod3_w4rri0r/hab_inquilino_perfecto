const getDB = require('../../database/getDB');

const searchEstates = async (req, res, next) => {
  let connection;

  // Get search params from req body
  const reqFields = Object.keys(req.query);
  const reqValues = Object.values(req.query);

  try {
    // If there's no search params, throw an error
    if (reqFields.length === 0) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // First part of any searching SQL statement
    let searchQuery = `SELECT * FROM estates WHERE status = 'available'`;

    // Build the rest of the searching SQL statement depending on req params
    for (const reqField of reqFields) {
      switch (reqField) {
        case 'location':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        case 'minPrice':
          searchQuery += ` AND price >= ?`;
          break;

        case 'maxPrice':
          searchQuery += ` AND price <= ?`;
          break;

        case 'type':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        case 'minNumBedrooms':
          searchQuery += ` AND num_bedrooms >= ?`;
          break;

        case 'maxNumBedrooms':
          searchQuery += ` AND num_bedrooms <= ?`;
          break;

        case 'minNumToilettes':
          searchQuery += ` AND num_toilettes >= ?`;
          break;

        case 'maxNumToilettes':
          searchQuery += ` AND num_toilettes <= ?`;
          break;

        case 'furnished':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        case 'energyCertificate':
          searchQuery += ` AND energy_certificate = ?`;
          break;

        case 'balcony':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        case 'laundry':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        case 'parking':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        case 'storage_room':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        case 'pets':
          searchQuery += ` AND ${reqField} = ?`;
          break;

        default: {
          const error = new Error('Invalid request');
          error.httpStatus = 400;
          throw error;
        }
      }
    }

    // Get a connection to DB
    connection = await getDB();

    // Search in DB
    const estates = await connection.query(searchQuery, reqValues);

    // Determine data to include in response to client
    const message =
      estates[0].length > 0
        ? `${estates[0].length} searching results found`
        : 'No results were found';

    // Look for photos of every estate founded in last query
    let photos = [];
    for (let i = 0; i < estates[0].length; i++) {
      [photos] = await connection.query(
        `SELECT * FROM photos WHERE idEstate = ?`,
        estates[0][i].id
      );
      estates[0][i].photos = photos;
    }

    res.send({
      status: 'ok',
      message: message,
      data: estates[0],
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = searchEstates;
