const getDB = require('../../database/getDB');

/**
 * Function to edit/update a estate data in
 * database (DB) with data from client request
 *
 * @param {Object} req - Http request from client
 * @param {Object} res - Http response to client
 * @param {*} next
 */
const editEstate = async (req, res, next) => {
  let connection;

  let result = 'No fields were modified in DB';

  // Get estate id from request params
  const { idEstate } = req.params;

  // Fields and values to be modified (NEW) present in req body, if any
  const reqFields = Object.keys(req.body);
  const reqValues = Object.values(req.body);

  try {
    // If no fields in req body, throw an error
    if (reqFields.length === 0) {
      const error = new Error('Missing fields. No DB records were modified.');
      error.httpStatus = 400;
      throw error;
    }

    // Get DB connection
    connection = await getDB();

    // If a rental is still in course, no changes can be made, throw an error
    const [currentRentals] = await connection.query(
      `SELECT id FROM rentals WHERE idEstate = ? AND status = 'active'`,
      [idEstate]
    );
    if (currentRentals.length > 1) {
      const error = new Error(
        `ERROR: Impossible to modify estate. Check estate id and wether 
        there is an active rental related to it.
        `
      );
      error.httpStatus = 403;
      throw error;
    }

    // Otherwise, build the SQL statement (SELECT) to get fields to edit from DB
    let querySelect = `SELECT ${reqFields} FROM estates WHERE id = ?`;

    // Get fields to edit from DB (OLD)
    const estate = await connection.query(querySelect, [idEstate]);

    // Array with DB values
    const dbValues = Object.values(estate[0][0]);

    // New values to be stored in DB
    const newValues = [];
    const newFields = [];

    // If req (NEW) and DB (OLD) values are different, store those values
    for (let i = 0; i < reqValues.length; i++) {
      if (reqValues[i] !== dbValues[i]) {
        newValues.push(reqValues[i]);
        newFields.push(reqFields[i] + ' = ?');
      }
    }

    // Check if there are actually data to store in DB or not
    if (newFields.length > 0) {
      // Add field and value of modification date
      newFields.push('modifiedAt = ?');
      newValues.push(new Date());

      // Add id from estate as last value
      newValues.push(idEstate);

      // Update query statement
      const queryUpdate = `UPDATE estates SET ${newFields} WHERE id = ?`;

      // Update fields in DB
      await connection.query(queryUpdate, newValues);

      // Modify message to send to client in res (substract 1 due to field "modifiedAt")
      result = `${newFields.length - 1} field(s) updated in DB`;
    }

    // Send res to client
    res.send({
      status: 'ok',
      message: result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editEstate;
