const getDB = require('../../database/getDB');

const changeStatusEstates = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get from the params the id of the estate
    const { idEstate } = req.params;

    // Get from the body the new status
    const { newStatus } = req.body;

    //Possible Status
    const possibleStatus = ['available', 'hidden', 'rented'];

    const [results] = await connection.query(
      `SELECT * FROM estates WHERE id = ?`,
      [idEstate]
    );

    // If not exist...
    if (results.length < 1) {
      const error = new Error('Estate not found');
      error.httpStatus = 400;
      throw error;
    }

    //If not include...
    if (!possibleStatus.includes(newStatus)) {
      const error = new Error('Bad request');
      error.httpStatus = 400;
      throw error;
    }

    // Update the database
    await connection.query(
      `UPDATE estates SET status = ?, modifiedAt = ? WHERE id = ?
          `,
      [newStatus, new Date(), idEstate]
    );

    res.send({
      status: 'ok',
      message: 'The status of your property has been modified',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = changeStatusEstates;
