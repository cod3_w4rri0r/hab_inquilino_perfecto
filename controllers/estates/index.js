const newEstate = require('./newEstate');
const editEstate = require('./editEstate');
const deleteEstate = require('./deleteEstate');
const searchEstates = require('./searchEstates');
const changeStatusEstates = require('./changeStatusEstates');

module.exports = {
  newEstate,
  editEstate,
  deleteEstate,
  searchEstates,
  changeStatusEstates,
};
