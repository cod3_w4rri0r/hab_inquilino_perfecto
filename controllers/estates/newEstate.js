const getDB = require('../../database/getDB');
const { generateRandomString, sendMail, savePhoto } = require('../../helpers');
const { PUBLIC_HOST } = process.env;

const newEstate = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get de Id from token...
    const idReqUser = req.userAuth.id;

    // Get the properties of the body
    const {
      location,
      address,
      cp,
      price,
      type,
      num_bedrooms,
      num_toilettes,
      furnished,
      area,
      balcony,
      laundry,
      parking,
      storage_room,
      pets,
    } = req.body;

    const mandatoryParameters =
      !location &&
      !address &&
      !cp &&
      !price &&
      !type &&
      !num_bedrooms &&
      !num_toilettes &&
      !furnished &&
      !area &&
      !balcony &&
      !laundry &&
      !parking &&
      !storage_room &&
      !pets;

    // If any field is missing, throw an error
    if (mandatoryParameters) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    } else {
      // If the body has the necessary fields...
      //We generate a registration reference code.
      const estate_ref = generateRandomString(5);

      // We save the estate in the database
      const [newEstate] = await connection.query(
        `INSERT INTO estates (location,
          address,
          cp,
          price,
          type,
          num_bedrooms,
          num_toilettes,
          furnished,
          area,
          balcony,
          laundry,
          parking,
          storage_room,
          pets,
          estate_ref, 
          idUser, 
          createdAt) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
        [
          location,
          address,
          cp,
          price,
          type,
          num_bedrooms,
          num_toilettes,
          furnished,
          area,
          balcony,
          laundry,
          parking,
          storage_room,
          pets,
          estate_ref,
          idReqUser,
          new Date(),
        ]
      );

      // Get the user role
      const [[{ role }]] = await connection.query(
        `SELECT role FROM users WHERE id = ?`,
        [idReqUser]
      );

      // Get the rentals where user has been renter, if any
      const [rentals] = await connection.query(
        `SELECT id FROM rentals WHERE idRenter = ?`,
        [idReqUser]
      );

      // If user is renter
      if (role === 'renter') {
        // and has rentals (as renter), change role to hybrid
        if (rentals.length > 1) {
          await connection.query(
            `UPDATE users SET role = 'hybrid' WHERE id = ?`,
            [idReqUser]
          );
        } else {
          // else, change role to landlord
          await connection.query(
            `UPDATE users SET role = 'landlord' WHERE id = ?`,
            [idReqUser]
          );
        }
      }

      // Message we'll send to user email.
      const uploadEmail = `
        Felicitaciones! Has dado de alta tu propiedad en Inquilino Perfecto.
        Pulsa este link para regresar a la página: ${PUBLIC_HOST}}
      `;

      //We get the email address
      const email = await connection.query(
        `SELECT email FROM users WHERE id = ?`,
        [idReqUser]
      );

      // We send the email.
      await sendMail({
        to: email[0][0].email,
        subject: 'Alta de inmueble en INQUILINO PERFECTO',
        body: uploadEmail,
      });

      // Get id of estate we have just created
      const idEstate = newEstate.insertId;

      // Check wether "req.files" exists and wether it has one or more files (photos)
      if (req.files && Object.keys(req.files).length > 0) {
        // Go over all "req.files" values. We take only the first 15 of them. The rest, if any
        // will be ignored
        for (const photo of Object.values(req.files).slice(0, 15)) {
          // Store photo on server and get its name
          const photoName = await savePhoto(photo, 1);

          // Store photo in "photos" table
          await connection.query(
            `INSERT INTO photos (name, idEstate, createdAt) VALUES (?, ?, ?)`,
            [photoName, idEstate, new Date()]
          );
        }
      }

      res.send({
        status: 'ok',
        message: 'Estate registered successfully',
        registerCode: estate_ref,
      });
    }
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newEstate;
