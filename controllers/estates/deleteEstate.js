const getDB = require('../../database/getDB');
const { PUBLIC_HOST } = process.env;

const { sendMail } = require('../../helpers');

const deleteEstate = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    //Get the id of the property we want to delete/anonymize

    const { idEstate } = req.params;

    // We obtain the reference code of the estate
    const [results] = await connection.query(
      `SELECT * FROM estates WHERE id = ?`,
      [idEstate]
    );

    // If not exist...
    if (results.length < 1) {
      const error = new Error('Estate not found');
      error.httpStatus = 400;
      throw error;
    }

    // If already deleted...
    if (results[0].status === 'deleted') {
      const error = new Error('Property previously deleted...');
      error.httpStatus = 400;
      throw error;
    }

    // Update the status of the property.

    await connection.query(
      `UPDATE estates SET status = 'deleted', modifiedAt = ? WHERE id = ?
        `,
      [new Date(), idEstate]
    );

    // Message we'll send to user email
    const deleteEmail = `
      Has eliminado tu propiedad en Inquilino Perfecto.
      Pulsa este link para regresar a la página: ${PUBLIC_HOST}}
    `;

    // Get the user email
    const email = await connection.query(
      `SELECT email FROM users WHERE id = ?`,
      [req.userAuth.id]
    );

    // Send the email
    await sendMail({
      to: email,
      subject: 'Eliminación de inmueble en INQUILINO PERFECTO',
      body: deleteEmail,
    });

    res.send({
      status: 'ok',
      message: 'Estate deleted',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteEstate;
