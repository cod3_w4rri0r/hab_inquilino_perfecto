const getDB = require('../../database/getDB');

const deleteUsersRating = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the idRating of the user
    const { idRating } = req.params;

    // The requested rating is obtained from the users_rating table
    const [userRated] = await connection.query(
      `
      SELECT idRatedUser FROM users_ratings WHERE id = ?`,
      [idRating]
    );

    // We delete the rating
    await connection.query(`DELETE FROM users_ratings WHERE id = ?`, [
      idRating,
    ]);

    // Get current overall score from USER
    const [userRatings] = await connection.query(
      `SELECT *
      FROM users_ratings
      WHERE idRatedUser = ?`,
      [userRated[0].idRatedUser]
    );

    let updatedScore = 0;
    let totalRating = 0;

    // We calculate total sum of ratings (if there are)
    if (userRatings.length > 1) {
      for (const userRating of userRatings) {
        totalRating += Number(userRating.rating);
      }

      // Calculate the rating average or,
      updatedScore = totalRating / userRatings.length;
    } else {
      // if we have just deleted the last rating, average is 0
      updatedScore = 0;
    }

    await connection.query(
      `UPDATE users 
       SET score = ?
       WHERE id = ?`,
      [updatedScore, userRated[0].idRatedUser]
    );

    res.send({
      status: 'ok',
      message: 'User Rating Deleted',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteUsersRating;
