const getDB = require('../../database/getDB');
const { addDays, addMonths, isBefore } = require('date-fns');
const { generateRandomString, sendMail } = require('../../helpers');

const genEstateRatingCode = async (req, res, next) => {
  let connection;

  // Get rental id from request params
  const { idRental } = req.params;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get necessary parameters from rental
    const [[rental]] = await connection.query(
      `SELECT 
      idRenter, 
      use_date_estate_code,
      contract_start_date
      FROM rentals 
      WHERE id = ?`,
      [idRental]
    );

    // Check wether user is renter in this rental, if not, throw an error,
    if (req.userAuth.id !== rental.idRenter) {
      console.log('Not matching idUser and rental idRenter');
      const error = new Error('ERROR: Invalid rental id');
      error.httpStatus = 400;
      throw error;
    }
    // if so,
    // check wether an authorization code has already been used
    // if so, throw an error
    if (rental.use_date_estate_code) {
      console.log(
        'An authorization code to rate this estate has already been generated and used'
      );
      const error = new Error(
        'ERROR: Invalid estate rating authorization code'
      );
      error.httpStatus = 400;
      throw error;
    }
    // else,
    // check wether minimum period (3 months) has passed since rental start date,
    // if not, throw an error (ratings can only be done after 3 months)
    if (isBefore(new Date(), addMonths(rental.contract_start_date, 3))) {
      console.log('Less than 3 months from rental start');
      const error = new Error(
        'ERROR: Ratings can only be done after 3 months of rental'
      );
      error.httpStatus = 400;
      throw error;
    }

    // else,
    // generate a new rating authorization code and its expiration date
    const authCode = generateRandomString(5);
    const expDate = addDays(new Date(), 2);

    // and save it in rentals DB table
    await connection.query(
      `UPDATE rentals SET estate_rating_code = ?, exp_date_estate_code = ? WHERE id = ?`,
      [authCode, expDate, idRental]
    );

    // Get user email
    const [[{ email }]] = await connection.query(
      `SELECT email
       FROM users
       WHERE id = ?`,
      [req.userAuth.id]
    );

    // Build the email message
    const emailBody = `
      Se ha generado el siguiente código de valoración de inmueble: ${authCode}.
      Este código tiene una vigencia de 48 horas, tras las cuales quedará invalidado.
    `;

    // Send an email to user to provide the rating authorization code
    await sendMail({
      to: email,
      subject: 'INQUILINO PERFECTO. Código de autorización de valoración',
      body: emailBody,
    });

    res.send({
      status: 'ok',
      message: 'Rating Authorization code generated successfully',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = genEstateRatingCode;
