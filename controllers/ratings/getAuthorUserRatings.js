const getDB = require('../../database/getDB');

const getAuthorUserRatings = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the id of the author user (user who made the ratings)
    const { idAuthorUser } = req.params;

    // Get the possible query params.
    const { order, direction } = req.query;

    // Possible values for "order".
    const validOrderOptions = ['id', 'rating', 'comments', 'createdAt'];

    // Possible values for "direction"
    const valirDirectionOptions = ['desc', 'asc'];

    // Set the "order" and a default "order"
    const orderBy = validOrderOptions.includes(order) ? order : 'createdAt';

    // Set the "direction" and a default "direction"
    const orderDirection = valirDirectionOptions.includes(direction)
      ? direction
      : 'desc';

    // We get the ratings that the user has made
    const [ratings] = await connection.query(
      `
      SELECT id, rating, comments, createdAt FROM users_ratings WHERE idAuthorUser = ? 
      ORDER BY ${orderBy} ${orderDirection}
      `,
      [idAuthorUser]
    );

    res.send({
      status: 'ok',
      data: {
        users: ratings,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getAuthorUserRatings;
