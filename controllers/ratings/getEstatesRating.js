const getDB = require('../../database/getDB');

const getEstatesRating = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the id of the estate
    const { idEstate } = req.params;

    // Get the possible query params.
    const { order, direction } = req.query;

    // Possible values for "order".
    const validOrderOptions = [
      'id',
      'reliability_ad',
      'quality_materials',
      'luminosity',
      'comments',
      'createdAt',
    ];

    // Possible values for "direction"
    const valirDirectionOptions = ['desc', 'asc'];

    // Set the "order" and a default "order"
    const orderBy = validOrderOptions.includes(order) ? order : 'createdAt';

    // Set the "direction" and a default "direction"
    const orderDirection = valirDirectionOptions.includes(direction)
      ? direction
      : 'desc';

    // We get the ratings that the estate has
    const [ratings] = await connection.query(
      `
      SELECT id, reliability_ad, quality_materials, luminosity, comments, createdAt FROM estates_ratings WHERE idEstate = ? 
      ORDER BY ${orderBy} ${orderDirection}
      `,
      [idEstate]
    );

    res.send({
      status: 'ok',
      data: {
        estates: ratings,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getEstatesRating;
