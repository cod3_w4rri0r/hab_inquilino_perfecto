const getDB = require('../../database/getDB');

const editUsersRating = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the id of the user
    const { idRating } = req.params;

    // Get the possible body params
    let { rating, comments } = req.body;

    // Check if all fields are present in the request, if not it throws an error
    if (!rating && !comments) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Get the rating
    const [ratings] = await connection.query(
      `SELECT rating, comments, idUserRated FROM users_ratings WHERE id = ?`,
      [idRating]
    );

    // If we receive a new value we update it,
    // otherwise we keep the value of the database
    rating = rating || ratings[0].rating;
    comments = comments || ratings[0].comments;

    // Update the rating
    await connection.query(
      `UPDATE users_ratings SET rating = ?, comments = ?  WHERE id = ?`,
      [rating, comments, idRating]
    );

    // Get number of ratings for this user
    const [currentRatings] = await connection.query(
      `SELECT *
       FROM users_ratings
       WHERE idUserRated = ?`,
      [ratings[0].idUserRated]
    );

    let updatedScore = 0;
    let totalRating = 0;

    //it is verified that the array is not empty, therefore it does not have evaluation data
    if (currentRatings.length !== 0) {
      for (const currentRating of currentRatings) {
        totalRating += Number(currentRating.rating);
      }

      updatedScore = totalRating / currentRatings.length;
    } else {
      updatedScore = 0;
    }
    // The database is updated in the "Estates" table for the score column
    await connection.query(
      `UPDATE users 
       SET score = ?
       WHERE id = ?`,
      [updatedScore, ratings[0].idUserRated]
    );

    res.send({
      status: 'ok',
      message: 'User Rating Updated',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUsersRating;
