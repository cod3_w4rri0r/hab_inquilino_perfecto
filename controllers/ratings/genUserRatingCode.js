const getDB = require('../../database/getDB');
const { addDays, addMonths, isBefore } = require('date-fns');
const { generateRandomString, sendMail } = require('../../helpers');

const genUserRatingCode = async (req, res, next) => {
  let connection;

  let userType;

  // Get rental id from request params
  const { idRental } = req.params;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get necessary parameters from rental
    const [[rental]] = await connection.query(
      `SELECT 
      idRenter,
      idLandlord, 
      use_date_renter_code,
      use_date_landlord_code,
      contract_start_date
      FROM rentals 
      WHERE id = ?`,
      [idRental]
    );

    // Check wether user is renter or landlord in this rental, if not, throw an error
    if (req.userAuth.id === rental.idRenter) {
      userType = 'renter';
    } else if (req.userAuth.id === rental.idLandlord) {
      userType = 'landlord';
    } else {
      console.log('User not present in rental');
      const error = new Error('ERROR: Invalid rental id');
      error.httpStatus = 400;
      throw error;
    }

    // Check wether an authorization code has already been used
    // if so, throw an error
    if (userType === 'renter') {
      if (rental.use_date_landlord_code) {
        console.log(
          'An authorization code to rate this landlord has already been generated and used'
        );
        const error = new Error(
          'ERROR: Invalid user rating authorization code'
        );
        error.httpStatus = 400;
        throw error;
      }
    } else {
      if (rental.use_date_renter_code) {
        console.log(
          'An authorization code to rate this renter has already been generated and used'
        );
        const error = new Error(
          'ERROR: Invalid user rating authorization code'
        );
        error.httpStatus = 400;
        throw error;
      }
    }

    // Get id of user to be rated
    const idRatedUser =
      userType === 'renter' ? rental.idLandlord : rental.idRenter;

    // Make sure there's no other rating of this user about this user yet,
    const [ratingsByAuthor] = await connection.query(
      `SELECT id FROM users_ratings WHERE idRatedUser = ? AND idAuthorUser = ?`,
      [idRatedUser, req.userAuth.id]
    );
    // if so, throw an error
    if (ratingsByAuthor.length > 0) {
      console.log("There's already a rating made by author about this user");
      const error = new Error('Only one rating can be made about every user');
      error.httpStatus = 400;
      throw error;
    }

    // Check wether minimum period (3 months) has passed since rental start date,
    // if not, throw an error (ratings can only be done after 3 months)
    if (isBefore(new Date(), addMonths(rental.contract_start_date, 3))) {
      console.log('Less than 3 months from rental start');
      const error = new Error(
        'ERROR: Ratings can only be done after 3 months of rental'
      );
      error.httpStatus = 400;
      throw error;
    }

    // Generate a new rating authorization code and its expiration date,
    const authCode = generateRandomString(5);
    const expDate = addDays(new Date(), 2);

    // and save it in rentals DB table
    if (userType === 'renter') {
      await connection.query(
        `UPDATE rentals SET landlord_rating_code = ?, exp_date_landlord_code = ? WHERE id = ?`,
        [authCode, expDate, idRental]
      );
    } else {
      await connection.query(
        `UPDATE rentals SET renter_rating_code = ?, exp_date_renter_code = ? WHERE id = ?`,
        [authCode, expDate, idRental]
      );
    }

    // Get user email
    const [[{ email }]] = await connection.query(
      `SELECT email
       FROM users
       WHERE id = ?`,
      [req.userAuth.id]
    );

    // Build the email message
    const emailBody = `
      Se ha generado el siguiente código de valoración de usuario: ${authCode}.
      Este código tiene una vigencia de 48 horas, tras las cuales quedará invalidado.
    `;

    // Send an email to user to provide the rating authorization code
    await sendMail({
      to: email,
      subject: 'INQUILINO PERFECTO. Código de autorización de valoración',
      body: emailBody,
    });

    res.send({
      status: 'ok',
      message: 'Rating Authorization code generated successfully',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = genUserRatingCode;
