const getDB = require('../../database/getDB');

const deleteEstateRating = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the idRating of the estate
    const { idRating } = req.params;

    // The requested rating is obtained from the estates_rating table
    const [estateRated] = await connection.query(
      `
      SELECT idEstate FROM estates_ratings WHERE id = ?`,
      [idRating]
    );

    // We delete the rating
    await connection.query(`DELETE FROM estates_ratings WHERE id = ?`, [
      idRating,
    ]);

    // Get current overall score from Estate
    const [estatesRatings] = await connection.query(
      `SELECT *
      FROM estates_ratings
      WHERE idEstate = ?`,
      [estateRated[0].idEstate]
    );

    //Calculation of the average valuation of the property

    let updatedRating = 0;
    let totalRating = 0;

    //it is verified that the array is not empty, therefore it does not have evaluation data
    if (estatesRatings.length !== 0) {
      for (const estatesRating of estatesRatings) {
        totalRating += Number(estatesRating.rating_avg);
      }

      updatedRating = totalRating / estatesRatings.length;
    } else {
      updatedRating = 0;
    }

    // The database is updated in the "Estates" table for the score column
    await connection.query(
      `UPDATE estates
       SET score = ?
       WHERE id = ?`,
      [updatedRating, estateRated[0].idEstate]
    );

    res.send({
      status: 'ok',
      message: 'Estate Rating Deleted',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteEstateRating;
