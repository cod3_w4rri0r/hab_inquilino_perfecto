const { isAfter } = require('date-fns');
const getDB = require('../../database/getDB');

/**
 * Function to add a rating/opinion about an estate
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const addEstatesRating = async (req, res, next) => {
  let connection;

  // Get rental id from request params
  const { idRental } = req.params;

  // Get id of user rating estate, from token
  const idUser = req.userAuth.id;

  // Get rating authorization code from request body
  const { ratingAuthCode: reqAuthCode } = req.body;

  try {
    // Get DB connection
    connection = await getDB();

    // Get rental's estate rating authorization code from DB
    const [[authCode]] = await connection.query(
      `SELECT estate_rating_code, exp_date_estate_code, idEstate FROM rentals WHERE id = ?`,
      [idRental]
    );

    // Check wether authorization code is ok and throw an error if not
    if (
      reqAuthCode !== authCode.estate_rating_code ||
      isAfter(new Date(), authCode.exp_date_estate_code)
    ) {
      const error = new Error('ERROR: Invalid rating authorization code');
      error.httpStatus = 400;
      throw error;
    }

    // Make sure there's no other rating of this user about this estate,
    const [ratingsAlreadyMade] = await connection.query(
      `SELECT id FROM estates_ratings WHERE idEstate = ? AND idUser = ?`,
      [authCode.idEstate, idUser]
    );
    // if so, throw an error
    if (ratingsAlreadyMade.length > 0) {
      const error = new Error('Only one rating can be made about every estate');
      error.httpStatus = 400;
      throw error;
    }

    // Get values from rating fields from request body
    const { reliabilityAd, qualityMaterials, luminosity, comments } = req.body;

    // Check wether all fields (except "comments" (optional)) are present in request, if not, throw an error
    if (!reliabilityAd || !qualityMaterials || !luminosity) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Get rating parameters average
    const ratingAvg = (reliabilityAd + qualityMaterials + luminosity) / 3;

    // Insert into DB this rating (Table: estates_ratings)
    await connection.query(
      `INSERT INTO estates_ratings (
        rating_avg, 
        reliability_ad, 
        quality_materials, 
        luminosity, 
        comments,
        idEstate,
        idUser,
        createdAt)
      VALUES(?,?,?,?,?,?,?,?)`,
      [
        ratingAvg,
        reliabilityAd,
        qualityMaterials,
        luminosity,
        comments,
        authCode.idEstate,
        idUser,
        new Date(),
      ]
    );

    // Get current overall score from estate
    const [[currentRating]] = await connection.query(
      `SELECT score
       FROM estates
       WHERE id = ?`,
      [authCode.idEstate]
    );

    // Get number of ratings for this estate
    const [ratingsNumber] = await connection.query(
      `SELECT rating_avg
       FROM estates_ratings
       WHERE idEstate = ?`,
      [authCode.idEstate]
    );

    // Set value of new rating of estate
    let newScore;

    // Check wether this is the first rating of estate or not
    // If not, recalculate score (average)
    // If so, rating is the score
    if (ratingsNumber.length > 1) {
      newScore =
        (currentRating.score * (ratingsNumber.length - 1) + ratingAvg) /
        ratingsNumber.length;
    } else {
      newScore = ratingAvg;
    }

    // Update score of estate
    await connection.query(
      `UPDATE estates 
       SET score = ?
       WHERE id = ?`,
      [newScore, authCode.idEstate]
    );

    // Invalidate estate rating auth code and record its use date
    await connection.query(
      `UPDATE rentals 
       SET estate_rating_code = ?, use_date_estate_code = ?
       WHERE id = ?`,
      [null, new Date(), idRental]
    );

    res.send({
      status: 'ok',
      message: 'Rating added to estate successfully!',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = addEstatesRating;
