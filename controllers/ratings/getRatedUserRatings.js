const getDB = require('../../database/getDB');

const getRatedUserRatings = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the id of the User Rated
    const { idRatedUser } = req.params;

    // Get the possible query params.
    const { order, direction } = req.query;

    // Possible values for "order".
    const validOrderOptions = ['id', 'rating', 'comments', 'createdAt'];

    // Possible values for "direction"
    const valirDirectionOptions = ['desc', 'asc'];

    // Set the "order" and a default "order"
    const orderBy = validOrderOptions.includes(order) ? order : 'createdAt';

    // Set the "direction" and a default "direction"
    const orderDirection = valirDirectionOptions.includes(direction)
      ? direction
      : 'desc';

    // We get the ratings that other users have made of this user (user making the request)
    const [ratings] = await connection.query(
      `
      SELECT id, rating, comments, createdAt FROM users_ratings WHERE idRatedUser = ? 
      ORDER BY ${orderBy} ${orderDirection}
      `,
      [idRatedUser]
    );

    res.send({
      status: 'ok',
      data: {
        users: ratings,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getRatedUserRatings;
