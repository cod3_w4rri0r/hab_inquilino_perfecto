const { isAfter } = require('date-fns');
const getDB = require('../../database/getDB');

/**
 * Function to add a rating/opinion about a user
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const addUsersRating = async (req, res, next) => {
  let connection;

  // Get id of user to be rated from request params
  const { idRental } = req.params;

  // Get values of ranking fields from request body
  const { reqAuthCode, rating, comments } = req.body;

  // Get rating's author user id from token
  const idAuthorUser = req.userAuth.id;

  try {
    // Check wether all fields (except "comments" (optional)) are present in request, if not, throw an error
    if (!rating || !reqAuthCode) {
      console.log('Missing fields');
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Get DB connection
    connection = await getDB();

    // Check the "role-in-rental" of user making the request
    const [[rentalMembers]] = await connection.query(
      `SELECT idRenter, idLandlord FROM rentals WHERE id = ?`,
      [idRental]
    );

    let role;

    if (idAuthorUser === rentalMembers.idRenter) {
      role = 'renter';
    } else if (idAuthorUser === rentalMembers.idLandlord) {
      role = 'landlord';
    } else {
      console.log('User does not belong to this rental');
      const error = new Error('ERROR: Invalid rental id');
      error.httpStatus = 400;
      throw error;
    }

    // Set variables depending on role
    const userCode =
      role === 'landlord' ? 'renter_rating_code' : 'landlord_rating_code';
    const expDate =
      role === 'landlord' ? 'exp_date_renter_code' : 'exp_date_landlord_code';
    const useDate =
      role === 'landlord' ? 'use_date_renter_code' : 'use_date_landlord_code';
    const idRatedUser =
      role === 'landlord' ? rentalMembers.idRenter : rentalMembers.idLandlord;

    // Get rental's user rating authorization code from DB
    const [[authCode]] = await connection.query(
      `SELECT ${userCode}, ${expDate}, ${idRatedUser}
      FROM rentals 
      WHERE id = ?`,
      [idRental]
    );

    // Set values of authorization code and expiration date saved in DB
    const dbAuthCode = authCode[userCode];
    const dbExpDate = authCode[expDate];

    // Check wether authorization code is ok and throw an error if not
    if (reqAuthCode !== dbAuthCode || isAfter(new Date(), dbExpDate)) {
      console.log('Invalid or expired rating authorization code');
      const error = new Error('ERROR: Invalid rating authorization code');
      error.httpStatus = 400;
      throw error;
    }

    // Make sure there's no other rating of this (author) user about this user (rated) yet,
    const [ratingsByAuthor] = await connection.query(
      `SELECT id FROM users_ratings WHERE idRatedUser = ? AND idAuthorUser = ?`,
      [idRatedUser, idAuthorUser]
    );
    // if so, throw an error
    if (ratingsByAuthor.length > 0) {
      console.log("There's already a rating made by author about this user");
      const error = new Error('Only one rating can be made about every user');
      error.httpStatus = 400;
      throw error;
    }

    // Insert into DB this rating (Table: users_ratings)
    await connection.query(
      `INSERT INTO users_ratings (
        rating,
        comments,
        idAuthorUser,
        idRatedUser,
        idRental,
        createdAt)
      VALUES(?,?,?,?,?,?)`,
      [rating, comments, idAuthorUser, idRatedUser, idRental, new Date()]
    );

    // Get current overall score from user
    const [[currentScore]] = await connection.query(
      `SELECT score
       FROM users
       WHERE id = ?`,
      [idRatedUser]
    );

    // Get number of ratings for this user
    const [ratingsNumber] = await connection.query(
      `SELECT rating
       FROM users_ratings
       WHERE idRatedUser = ?`,
      [idRatedUser]
    );

    // Set value of new score of user
    let newScore;

    // Check wether this is the first rating about user or not
    // If not, recalculate score (average)
    // If so, rating is the score
    if (ratingsNumber.length > 1) {
      newScore =
        (currentScore.score * (ratingsNumber.length - 1) + rating) /
        ratingsNumber.length;
    } else {
      newScore = rating;
    }

    // Update user score in DB
    await connection.query(
      `UPDATE users 
       SET score = ?
       WHERE id = ?`,
      [newScore, idRatedUser]
    );

    // Invalidate user rating auth code and record its use date
    await connection.query(
      `UPDATE rentals 
       SET ${userCode} = ?, ${useDate} = ?
       WHERE id = ?`,
      [null, new Date(), idRental]
    );

    res.send({
      status: 'ok',
      message: 'Rating added to user successfully!',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = addUsersRating;
