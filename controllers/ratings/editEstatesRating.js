const getDB = require('../../database/getDB');

const editEstatesRating = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the id of the estate
    const { idRating } = req.params;

    // Get the possible body params
    let { reliability_ad, quality_materials, luminosity, comments } = req.body;

    // Check if all fields are present in the request, if not it throws an error
    if (!reliability_ad && !quality_materials && !luminosity && !comments) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // get the rating
    const [rating] = await connection.query(
      `SELECT * FROM estates_ratings WHERE id = ?`,
      [idRating]
    );

    // If we receive a new value we update it,
    // otherwise we keep the value of the database
    reliability_ad = Number(reliability_ad) || rating[0].reliability_ad;
    quality_materials =
      Number(quality_materials) || rating[0].quality_materials;
    luminosity = Number(luminosity) || rating[0].luminosity;
    comments = comments || rating[0].comments;

    const rating_avg = (reliability_ad + quality_materials + luminosity) / 3;

    // Update the rating
    await connection.query(
      `UPDATE estates_ratings 
      SET rating_avg = ?, 
      reliability_ad = ?, 
      quality_materials = ?, 
      luminosity = ?, 
      comments = ? 
      WHERE id = ?`,
      [
        rating_avg,
        reliability_ad,
        quality_materials,
        luminosity,
        comments,
        idRating,
      ]
    );

    // Get number of ratings for this user
    const [currentRatings] = await connection.query(
      `SELECT *
      FROM estates_ratings
      WHERE idEstate = ?`,
      [rating[0].idEstate]
    );

    //Calculation of the average valuation of the property
    let updatedScore = 0;
    let totalRating = 0;

    //it is verified that the array is not empty, therefore it does not have evaluation data
    if (currentRatings.length !== 0) {
      for (const currentRating of currentRatings) {
        totalRating += Number(currentRating.rating_avg);
      }

      updatedScore = totalRating / currentRatings.length;
    } else {
      updatedScore = 0;
    }

    // The database is updated in the "Estates" table for the score column
    await connection.query(
      `UPDATE estates 
      SET score = ?
      WHERE id = ?`,
      [updatedScore, rating[0].idEstate]
    );

    res.send({
      status: 'ok',
      message: 'Estate Rating Updated',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editEstatesRating;
