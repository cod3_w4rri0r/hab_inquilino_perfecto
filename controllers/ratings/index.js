const addEstatesRating = require('./addEstatesRating');
const getEstatesRating = require('./getEstatesRating');
const editEstatesRating = require('./editEstatesRating');
const editUsersRating = require('./editUsersRating');
const getAuthorUserRatings = require('./getAuthorUserRatings');
const getRatedUserRatings = require('./getRatedUserRatings');
const addUsersRatings = require('./addUserRatings');
const genEstateRatingCode = require('./genEstateRatingCode');
const deleteUserRating = require('./deleteUserRating');
const deleteEstateRating = require('./deleteEstateRating');
const genUserRatingCode = require('./genUserRatingCode');

module.exports = {
  genEstateRatingCode,
  addEstatesRating,
  addUsersRatings,
  getEstatesRating,
  getRatedUserRatings,
  getAuthorUserRatings,
  editEstatesRating,
  editUsersRating,
  deleteUserRating,
  deleteEstateRating,
  genUserRatingCode,
};
