const getDB = require('../../database/getDB');

/**
 * Function to provide estates locations of registered
 * estates of the platform, to suggest and to show where
 * users can find estates availables to rent
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const getLocations = async (req, res, next) => {
  let connection;
  const locationsList = [];

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the different locations that already exist in DB; table estates
    // (returns an array of objects)
    const locations = await connection.query(
      `SELECT DISTINCT location FROM estates`
    );

    // Get locations into an array of locations (strings)
    locations[0].map((location) => locationsList.push(location.location));

    res.send({
      status: 'ok',
      locationsList,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getLocations;
