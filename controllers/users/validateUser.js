const getDB = require('../../database/getDB');

const validateUser = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the registration code
    const { registrationCode } = req.params;

    // We check if there is any user pending to validate with the previous code
    const [users] = await connection.query(
      `SELECT id FROM users WHERE registrationCode = ?`,
      [registrationCode]
    );

    // If there are no users pending validation with that registration code, we throw an error
    if (users.length < 1) {
      const error = new Error(
        'There are no pending users to validate with that registration code'
      );
      error.httpStatus = 404;
      throw error;
    }

    // We activate the user and remove the registration code.
    await connection.query(
      `UPDATE users SET active = true, registrationCode = NULL WHERE registrationCode = ?`,
      [registrationCode]
    );

    res.send({
      status: 'ok',
      message: 'User activated',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = validateUser;
