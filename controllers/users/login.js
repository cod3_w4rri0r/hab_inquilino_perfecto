const getDB = require('../../database/getDB');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const login = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the email and password
    const { email, password } = req.body;

    // If any field is missing we throw an error
    if (!email || !password) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // We check if there is a user with that email
    const [users] = await connection.query(
      `SELECT id, role, password, active FROM users WHERE email = ?`,
      [email]
    );

    // Variable that will store a boolean value: correct or incorrect password
    let validPassword;

    // In case there is a user with the email established in the body, we check
    //if the password is correct.
    if (users.length > 0) {
      validPassword = await bcrypt.compare(password, users[0].password);
    }

    // If the password is not correct we throw an error
    if (users.length < 1 || !validPassword) {
      const error = new Error('Incorrect email or password');
      error.httpStatus = 401;
      throw error;
    }

    // If the user exists but is not active we throw an error
    if (!users[0].active) {
      const error = new Error('User activation pending');
      error.httpStatus = 401;
      throw error;
    }

    // Object with the information that we are going to pass to the token.
    const tokenInfo = {
      id: users[0].id,
      role: users[0].role,
    };

    // We create the token.
    const token = jwt.sign(tokenInfo, process.env.SECRET, {
      expiresIn: '1d',
    });

    // Register access to platform (login registry)
    await connection.query(
      `INSERT INTO log_reg (
        idUser,
        login_datetime)
        VALUES(?,?)
      `,
      [users[0].id, new Date()]
    );

    res.send({
      status: 'ok',
      data: {
        token,
        id: users[0].id,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = login;
