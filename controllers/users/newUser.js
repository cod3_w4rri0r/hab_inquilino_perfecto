const bcrypt = require('bcrypt');
const saltRounds = 10;
const getDB = require('../../database/getDB');
const { generateRandomString, sendMail } = require('../../helpers');
const { PUBLIC_HOST } = process.env;

const newUser = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the necessary fields from the body.
    let {
      email,
      password,
      name,
      surname,
      dni_nie,

      bio,
      role,
    } = req.body;

    // We generate a one-time registration code.
    const registrationCode = generateRandomString(40);

    // We hashed the password
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    // We save the user in the database
    await connection.query(
      `INSERT INTO users (
        email, 
        password, 
        name,
        surname,
        
        dni_nie,
        bio,
        role,
        registrationCode, 
        createdAt) 
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
      [
        email,
        hashedPassword,
        name,
        surname,

        dni_nie,
        bio,
        role,
        registrationCode,
        new Date(),
      ]
    );

    // Message that we will send to the user's email
    const emailBody = `
            You just signed up for INQUILINO PERFECTO
            Click this link to verify your account: ${PUBLIC_HOST}/users/validate/${registrationCode}.
        `;

    // We send the email.
    await sendMail({
      to: email,
      subject: 'Activate your user in INQUILINO PERFECTO.',
      body: emailBody,
    });

    res.send({
      status: 'ok',
      message: 'Registered user, check your email to activate it',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newUser;
