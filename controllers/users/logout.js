const { differenceInMilliseconds } = require('date-fns');
const getDB = require('../../database/getDB');

const logout = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    // Check there's already an entry in log_reg table of
    // DB for this user last login, if not, throw an error
    const [lastLogin] = await connection.query(
      `SELECT id, login_datetime, logout_datetime FROM log_reg WHERE idUser = ?`,
      [req.userAuth.id]
    );
    if (
      !lastLogin[lastLogin.length - 1].login_datetime ||
      (lastLogin[lastLogin.length - 1].login_datetime &&
        lastLogin[lastLogin.length - 1].logout_datetime)
    ) {
      console.log('User has not logged in');
      const error = new Error('ERROR: Operation not available');
      error.httpStatus = 400;
      throw error;
    }

    // Calculate difference in time
    // const days = differenceInDays(new Date(), login_datetime);
    // const hours = differenceInHours(new Date(), login_datetime);
    // const minutes = differenceInMinutes(new Date(), login_datetime);
    // const seconds = differenceInSeconds(new Date(), login_datetime);
    // const millisecs = differenceInMilliseconds(new Date(), login_datetime);

    const currentDatetime = new Date();

    const diffMillis = differenceInMilliseconds(
      currentDatetime,
      lastLogin[lastLogin.length - 1].login_datetime
    );
    const diffSecs = Math.round(diffMillis / 1000);
    const diffMins = Math.round(diffSecs / 60);
    const diffHours = Math.round(diffMins / 60);
    const diffDays = Math.round(diffHours / 24);

    // Store logout data in DB
    await connection.query(
      `UPDATE log_reg SET logout_datetime = ?,
       diff_days = ?,
       diff_hours = ?,
       diff_minutes = ?,
       diff_seconds = ?,
       diff_millis = ?
       WHERE id = ?
       `,
      [
        currentDatetime,
        diffDays,
        diffHours,
        diffMins,
        diffSecs,
        diffMillis,
        lastLogin[lastLogin.length - 1].id,
      ]
    );

    res.send({
      status: 'ok',
      message: `User ${req.userAuth.id} logged out successfully!`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = logout;
