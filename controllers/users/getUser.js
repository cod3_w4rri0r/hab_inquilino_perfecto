const getDB = require('../../database/getDB');

const getUser = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get id from user we want to get info
    const { idUser } = req.params;

    // Get id from user who makes the request
    const idReqUser = req.userAuth.id;

    // Get all data from user we're interested in
    const [users] = await connection.query(
      `SELECT id, name, surname, avatar, bio, role, dni_nie, email, createdAt FROM users WHERE id = ?`,
      [idUser]
    );

    // Object with basic info (for other users)
    const userInfo = {
      name: users[0].name,
      surname: users[0].surname,
      avatar: users[0].avatar,
      bio: users[0].bio,
      role: users[0].role,
    };

    // If user making the request is admin role or the same user we're
    // looking for (user making a request over himself), we add more fields
    if (users[0].id === idReqUser || req.userAuth.role === 'admin') {
      userInfo.email = users[0].email;
      userInfo.dni = users[0].dni_nie;
      userInfo.createdAt = users[0].createdAt;
    }

    res.send({
      status: 'ok',
      data: {
        user: userInfo,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getUser;
