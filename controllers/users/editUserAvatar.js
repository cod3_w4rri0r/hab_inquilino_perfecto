const getDB = require('../../database/getDB');

const { savePhoto, deletePhoto } = require('../../helpers');

const editUserAvatar = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the id of the user we want to edit
    const { idUser } = req.params;

    // If the property "req.files.avatar" does not exist we throw an error.
    // Given that this property might not exist we have to first check if it exists
    // before checking if "req.files.avatar" exists
    if (!(req.files && req.files.avatar)) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // We get the avatar of the current user
    const [users] = await connection.query(
      `SELECT avatar FROM users WHERE id = ?`,
      [idUser]
    );

    // We check if the user we want to edit already has an avatar.
    if (users[0].avatar) {
      // We delete the avatar from the disk (from the server).
      await deletePhoto(users[0].avatar);
    }

    // We save the new avatar on the server and get its name
    const avatarName = await savePhoto(req.files.avatar, 0);

    // We update the user with the name of the new avatar
    await connection.query(
      `UPDATE users SET avatar = ?, modifiedAt = ? WHERE id = ?`,
      [avatarName, new Date(), idUser]
    );

    res.send({
      status: 'ok',
      message: "User's avatar updated",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUserAvatar;
