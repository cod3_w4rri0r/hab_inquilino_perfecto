const editUser = require('./editUser');
const editUserAvatar = require('./editUserAvatar');
const editUserPass = require('./editUserPass');
const getUser = require('./getUser');
const login = require('./login');
const newUser = require('./newUser');
const recoverUserPass = require('./recoverUserPass');
const validateUser = require('./validateUser');
const resetUserPass = require('./resetUserPass');
const deleteUser = require('./deleteUser');
const logout = require('./logout');
const getUserEstates = require('./getUserEstates');

module.exports = {
  newUser,
  validateUser,
  login,
  logout,
  getUser,
  editUser,
  editUserPass,
  editUserAvatar,
  recoverUserPass,
  resetUserPass,
  deleteUser,
  getUserEstates,
};
