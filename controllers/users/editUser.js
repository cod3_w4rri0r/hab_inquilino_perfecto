const getDB = require('../../database/getDB');

const editUser = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // id from user to be edited
    const { idUser } = req.params;

    // Get new bio from request body
    const { newBio } = req.body;

    // If no bio received, throw an error
    if (!newBio) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Otherwise, update the user bio
    await connection.query(
      `UPDATE users SET bio = ?, modifiedAt = ? WHERE id = ?`,
      [newBio, new Date(), idUser]
    );

    // Send a response to front
    res.send({
      status: 'ok',
      message: 'User updated',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUser;
