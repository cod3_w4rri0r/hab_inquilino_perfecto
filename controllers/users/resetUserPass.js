const getDB = require('../../database/getDB');

const bcrypt = require('bcrypt');
const saltRounds = 10;

const resetUserPass = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the recovery code and the new password
    const { recoverCode, newPassword } = req.body;

    // If any field is missing we throw an error.
    if (!recoverCode || !newPassword) {
      const error = new Error('Missing fields!');
      error.httpStatus = 400;
      throw error;
    }

    // We get the user with that recovery code
    const [users] = await connection.query(
      `SELECT id FROM users WHERE recoverCode = ?`,
      [recoverCode]
    );

    // If there is no user with that recovery code, we launch an error.
    if (users.length < 1) {
      const error = new Error('Wrong recovery code');
      error.httpStatus = 404;
      throw error;
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(newPassword, saltRounds);

    // We update the password of the user who has that recovery code
    await connection.query(
      `UPDATE users SET password = ?, recoverCode = NULL, modifiedAt = ? WHERE id = ?`,
      [hashedPassword, new Date(), users[0].id]
    );

    res.send({
      status: 'ok',
      message: 'Password updated',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = resetUserPass;
