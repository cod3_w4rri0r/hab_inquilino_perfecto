const getDB = require('../../database/getDB');

const { deletePhoto, generateRandomString } = require('../../helpers');

const deleteUser = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the id of the user we want to delete / anonymize.
    const { idUser } = req.params;

    // If the id of the user we want to delete is the main administrator (id: 1),
    // we throw an error.
    if (Number(idUser) === 1) {
      const error = new Error('Main admin cannot be deleted');
      error.httpStatus = 403;
      throw error;
    }

    // We get the avatar of the user.
    const [users] = await connection.query(
      `SELECT avatar FROM users WHERE id = ?`,
      [idUser]
    );

    // If the user has an avatar we delete it from the server
    if (users[0].avatar) {
      await deletePhoto(users[0].avatar);
    }

    // We anonymize the user.
    await connection.query(
      `
        UPDATE users
        SET email = "[deleted]", password = ?, name = "[deleted]", surname = "[deleted]", 
        dni_nie = NULL, avatar = NULL, bio = "[deleted]", active = 0, deleted = 1, modifiedAt = ?
        WHERE id = ?
      `,
      [generateRandomString(20), new Date(), idUser]
    );

    res.send({
      status: 'ok',
      message: 'User deleted',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteUser;
