const getDB = require('../../database/getDB');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const editUserPass = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the id of the user we want to edit.
    const { idUser } = req.params;

    // We get the old password and the new one.
    const { oldPassword, newPassword } = req.body;

    // We get the user's password
    const [users] = await connection.query(
      `SELECT password FROM users WHERE id = ?`,
      [idUser]
    );

    // We store a boolean value in a variable: correct or incorrect password
    const isValid = await bcrypt.compare(oldPassword, users[0].password);

    // If the password is incorrect we throw an error
    if (!isValid) {
      const error = new Error('Incorrect password');
      error.httpStatus = 401;
      throw error;
    }

    // Hash the new password
    const hashedPassword = await bcrypt.hash(newPassword, saltRounds);

    // We update the database
    await connection.query(
      `UPDATE users SET password = ?, modifiedAt = ? WHERE id = ?`,
      [hashedPassword, new Date(), idUser]
    );

    res.send({
      status: 'ok',
      message: 'Password updated',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editUserPass;
