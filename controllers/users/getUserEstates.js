const getDB = require('../../database/getDB');

const getUserEstates = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    const { idUser } = req.params;

    const [estates] = await connection.query(
      `SELECT * FROM estates WHERE idUser = ?`,
      [idUser]
    );

    const results =
      estates.length > 1
        ? estates
        : 'No estates belonging to this user were found';

    res.send({
      status: 'ok',
      data: results,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getUserEstates;
