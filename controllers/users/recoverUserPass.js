const getDB = require('../../database/getDB');

const { generateRandomString, sendMail } = require('../../helpers');

const recoverUserPass = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the email.
    const { email } = req.body;

    // If the email is missing we throw an error.
    if (!email) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // We check if the email exists in the database
    const [users] = await connection.query(
      `SELECT id FROM users WHERE email = ?`,
      [email]
    );

    // If there is a user with that email, we send you a recovery code
    if (users.length > 0) {
      // We generate a recovery code.
      const recoverCode = generateRandomString(20);

      // We create the body of the message.
      const emailBody = `
                
        A password change was requested for the user registered with this email in the app INQUILINO PERFECTO.

        The recovery code is: ${recoverCode}.

        If it wasn't you ignore this email.
      `;

      // We send the email.
      await sendMail({
        to: email,
        subject: 'Change password in INQUILINO PERFECTO',
        body: emailBody,
      });

      // We add the recovery code to the user with said email
      await connection.query(
        `UPDATE users SET recoverCode = ?, modifiedAt = ? WHERE email = ?`,
        [recoverCode, new Date(), email]
      );
    }

    res.send({
      status: 'ok',
      message: 'If the email exists, a recovery code has been sent',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = recoverUserPass;
