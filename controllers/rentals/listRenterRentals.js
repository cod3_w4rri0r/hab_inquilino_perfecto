const getDB = require('../../database/getDB');

const listRenterRentals = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the possible query params.
    const { order, direction } = req.query;

    // Possible values for "order".
    const validOrderOptions = [
      'status',
      'contract_start_date',
      'contract_end_date',
    ];

    // Possible values for "direction"
    const valirDirectionOptions = ['desc', 'asc'];

    // Set the "order" and a default "order"
    const orderBy = validOrderOptions.includes(order)
      ? order
      : 'contract_start_date';

    // Set the "direction" and a default "direction"
    const orderDirection = valirDirectionOptions.includes(direction)
      ? direction
      : 'desc';

    // We get the rentals that the renter has
    const [rentals] = await connection.query(
      `
      SELECT id, status, contract_start_date, contract_end_date FROM rentals WHERE idRenter = ? 
      ORDER BY ${orderBy} ${orderDirection}
      `,
      [req.userAuth.id]
    );

    res.send({
      status: 'ok',
      data: {
        rental: rentals,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = listRenterRentals;
