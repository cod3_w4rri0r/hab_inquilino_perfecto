const getDB = require('../../database/getDB');

const requestResponse = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    //Get the properties of the body
    const { responseLandlord, idRentalReq } = req.body;

    // If any field is missing, throw an error
    if (!responseLandlord || !idRentalReq) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Search the rental request
    const [request] = await connection.query(
      `SELECT id, idLandlord FROM rentals_requests WHERE id = ?`,
      [idRentalReq]
    );

    if (request.length === 0) {
      const error = new Error(
        'No rental requests for this estate by this user'
      );
      error.httpStatus = 404;
      throw error;
    }

    // Check wether the user resolving the rental request is actually the
    // estate owner or not
    if (request[0].idLandlord !== req.userAuth.id) {
      const error = new Error('ERROR: You do not have enough permissions');
      error.httpStatus = 400;
      throw error;
    }

    // If the response is other than accepted or rejected, throw the error
    if (responseLandlord !== 'accepted' && responseLandlord !== 'rejected') {
      const error = new Error('Bad answer...');
      error.httpStatus = 400;
      throw error;
    }

    // Update the DB
    await connection.query(
      `UPDATE rentals_requests SET status = ?, resolution_date = ?
      WHERE id = ?`,
      [responseLandlord, new Date(), request[0].id]
    );

    res.send({
      status: `${responseLandlord}`,
      message: 'Request resolved successfully',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = requestResponse;
