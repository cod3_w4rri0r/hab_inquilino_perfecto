const getDB = require('../../database/getDB');
const { differenceInWeeks, differenceInDays, isBefore } = require('date-fns');
const { generateRandomString } = require('../../helpers');

const newRentalRequest = async (req, res, next) => {
  let connection;

  // Get renter id from token
  const idRenter = req.userAuth.id;

  // Get estate id from req body
  const { idEstate } = req.body;

  try {
    // Check wether all fields were received
    if (!idEstate) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Get a connection to DB
    connection = await getDB();

    // Check if estate is actually available
    const [[estateStatus]] = await connection.query(
      `SELECT status FROM estates WHERE id = ?`,
      [idEstate]
    );
    if (estateStatus.status !== 'available') {
      const error = new Error('ERROR: Unavailable estate');
      error.httpStatus = 400;
      throw error;
    }

    // Get id of estate's landlord
    const [idLandlord] = await connection.query(
      `SELECT idUser FROM estates WHERE id = ?`,
      [idEstate]
    );

    // Check wether user requesting the estate is not the
    // estate owner (landlord)
    if (idLandlord[0].idUser === req.userAuth.id) {
      console.log('Renter and landlord are the same user');
      const error = new Error('ERROR: Invalid request');
      error.httpStatus = 400;
      throw error;
    }

    // Check wether there is already a pending rental request
    // to the estate by the same user
    let [existingRenterRequests] = await connection.query(
      `SELECT * FROM rentals_requests 
      WHERE idRenter = ? AND idEstate = ? AND status = 'pending'`,
      [idRenter, idEstate]
    );

    // If so, throw an error
    if (existingRenterRequests.length !== 0) {
      const error = new Error('Request still pending for this estate');
      error.httpStatus = 400;
      throw error;
    }

    // Otherwise,
    // Check wether there are previously rejected requests to this estate
    // from this user
    [existingRenterRequests] = await connection.query(
      `SELECT * FROM rentals_requests 
      WHERE idRenter = ? AND idEstate = ? AND status = 'rejected'`,
      [idRenter, idEstate]
    );

    // If so, if it was made less than a week ago, throw an error
    if (existingRenterRequests.length !== 0) {
      if (
        differenceInWeeks(
          new Date(),
          existingRenterRequests[0].resolution_date
        ) < 1
      ) {
        const diffInDays = differenceInDays(
          new Date(),
          existingRenterRequests[0].resolution_date
        );
        console.log(
          'Estate requested by the same renter less than a week ago. (D_Diff = ',
          diffInDays,
          ')'
        );
        const error = new Error(
          'ERROR: Too short time between following requests for this estate'
        );
        error.httpStatus = 400;
        throw error;
      }
    }

    // If not,
    // check wether there is already an accepted request but not a rental yet
    [existingRenterRequests] = await connection.query(
      `SELECT * FROM rentals_requests
      WHERE idRenter = ? AND idEstate = ? AND status = 'accepted'`,
      [idRenter, idEstate]
    );
    if (existingRenterRequests.length > 0) {
      const [relatedRental] = await connection.query(
        `SELECT id, contract_end_date FROM rentals
        WHERE request_ref = ?`,
        [existingRenterRequests[0].request_ref]
      );
      if (relatedRental.length === 0) {
        console.log(
          'There is an accepted rental request of this user in this same estate'
        );
        const error = new Error('ERROR: Rental pending on a previous request');
        error.httpStatus = 400;
        throw error;
      } else {
        // There's a rental related to an accepted rental request
        if (isBefore(new Date(), relatedRental[0].contract_end_date)) {
          console.log(
            'Existing current rental relating this user and this estate'
          );
          const error = new Error('ERROR: Unavailable rental request');
          error.httpStatus = 400;
          throw error;
        }
      }
    }

    // Else (Rental request/s with a related finished rental or no requests at all),
    // update the rental requests table with the request of the user
    await connection.query(
      `INSERT INTO rentals_requests (
        idRenter,
        idLandlord,
        idEstate,
        createdAt, 
        status, 
        request_ref)
        VALUES(?,?,?,?,?,?)
      `,
      [
        idRenter,
        idLandlord[0].idUser,
        idEstate,
        new Date(),
        'pending',
        generateRandomString(5),
      ]
    );

    res.send({
      status: 'ok',
      message: `Successful rental request to estate with id: ${idEstate}!`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newRentalRequest;
