const getDB = require('../../database/getDB');
const { generateRandomString } = require('../../helpers');

const newRental = async (req, res, next) => {
  let connection;

  // Get id from user (landlord) who's creating the rental
  const idLandlord = req.userAuth.id;

  // Get rental request id from req params
  const { idRentalReq } = req.params;

  // Get parameters of rental
  const { startDate, endDate, type } = req.body;

  try {
    // Check wether all fields are present in request, if not, throw an error
    if (!idRentalReq || !startDate || !endDate || !type) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Get a connection to DB
    connection = await getDB();

    // Get data from rental request
    const [[request]] = await connection.query(
      `SELECT idRenter, idEstate, request_ref, status
       FROM rentals_requests 
       WHERE id = ?`,
      [idRentalReq]
    );

    // Check wether user sending the request is actually the estate's owner
    const [[estate]] = await connection.query(
      `SELECT idUser FROM estates WHERE id = ?`,
      [request.idEstate]
    );
    if (estate.idUser !== idLandlord) {
      console.log("User is not this estate's owner");
      const error = new Error("ERROR: You don't have enough permissions");
      error.httpStatus = 400;
      throw error;
    }

    // Check wether renter and landlord are not the same, otherwise throw an error
    if (idLandlord === request.idRenter) {
      const error = new Error('ERROR: Landlord cannot rent his own estates');
      error.httpStatus = 400;
      throw error;
    }

    // Check if rental request is already accepted, if not, throw an error
    if (request.status !== 'accepted') {
      const error = new Error('ERROR: Rental request still pending');
      error.httpStatus = 400;
      throw error;
    }

    // Check there's no rentals of this estate still active, otherwise throw an error
    const [currentRentals] = await connection.query(
      `SELECT id FROM rentals WHERE idEstate = ? AND status = 'active'`,
      [request.idEstate]
    );
    if (currentRentals.length > 0) {
      const error = new Error('ERROR: This estate is not available');
      error.httpStatus = 400;
      throw error;
    }

    // Else, create the new rental
    await connection.query(
      `INSERT INTO rentals (
      idRenter,
      idLandlord, 
      idEstate, 
      status, 
      contract_start_date, 
      contract_end_date, 
      type, 
      requests_ref, 
      contract_ref,
      createdAt)
      VALUES(?,?,?,?,?,?,?,?,?,?)`,
      [
        request.idRenter,
        idLandlord,
        request.idEstate,
        'active',
        startDate,
        endDate,
        type,
        request.request_ref,
        generateRandomString(5),
        new Date(),
      ]
    );

    // If renter's role is "landlord", change role to "hybrid"
    await connection.query(`UPDATE users SET role = 'hybrid' WHERE id = ?`, [
      request.idRenter,
    ]);

    // Change estate's status
    await connection.query(`UPDATE estates SET status = ? WHERE id = ?`, [
      'rented',
      request.idEstate,
    ]);

    res.send({
      status: 'ok',
      message: 'New rental created successfully',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = newRental;
