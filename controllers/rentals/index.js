const deleteRentalRequest = require('./deleteRentalRequest');
const editRental = require('./editRental');
const newRental = require('./newRental');
const newRentalRequest = require('./newRentalRequest');
const requestResponse = require('./requestResponse');
const listRenterRentals = require('./listRenterRentals');
const listLandlordContracts = require('./listLandlordContracts');

module.exports = {
  newRentalRequest,
  deleteRentalRequest,
  newRental,
  editRental,
  requestResponse,
  listRenterRentals,
  listLandlordContracts,
};
