const getDB = require('../../database/getDB');

const deleteRentalRequest = async (req, res, next) => {
  let connection;

  // Get renter id from token
  const idRenter = req.userAuth.id;

  // Get estate id from req body
  const { idEstate } = req.body;

  // Check wether all fields were received
  if (!idEstate) {
    const error = new Error('Missing fields');
    error.httpStatus = 400;
    throw error;
  }

  try {
    // Get a database connection
    connection = await getDB();

    // Check wether there is actually any record in DB to delete
    const [[request]] = await connection.query(
      `SELECT id FROM rentals_requests WHERE idRenter = ? AND idEstate = ? AND status = 'pending'`,
      [idRenter, idEstate]
    );

    if (!request) {
      const error = new Error('ERROR: No requests for this estate and user');
      error.httpStatus = 404;
      throw error;
    }

    // If so, cancel it
    await connection.query(
      `UPDATE rentals_requests
      SET status = 'cancelled', modifiedAt = ?
      WHERE id = ${request.id}`,
      [new Date()]
    );

    res.send({
      status: 'ok',
      message: `Rental request (id: ${request.id}) deleted from DB. [Renter: ${idRenter}; Estate: ${idEstate}]`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteRentalRequest;
