const getDB = require('../../database/getDB');

const listLandlordContracts = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get the possible query params
    const { order, direction } = req.query;

    // Possible values for "order"
    const validOrderOptions = ['status', 'createdAt'];

    // Possible values for "direction"
    const valirDirectionOptions = ['desc', 'asc'];

    // Set the "order" and a default "order"
    const orderBy = validOrderOptions.includes(order) ? order : 'createdAt';

    // Set the "direction" and a default "direction"
    const orderDirection = valirDirectionOptions.includes(direction)
      ? direction
      : 'desc';

    // We get the contracts that the Landlord has
    const [contracts] = await connection.query(
      `
        SELECT id, status FROM rentals WHERE idLandlord = ? 
        ORDER BY ${orderBy} ${orderDirection}
      `,
      [req.userAuth.id]
    );

    res.send({
      status: 'ok',
      data: {
        contract: contracts,
      },
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = listLandlordContracts;
