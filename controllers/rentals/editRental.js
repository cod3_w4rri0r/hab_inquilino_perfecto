const getDB = require('../../database/getDB');

const editRental = async (req, res, next) => {
  let connection;

  // Get rental id from path params
  const { idRental } = req.params;

  // Get parameters of rental
  const { status, startDate, endDate, type } = req.body;

  try {
    // Check wether all fields are present in request, if not, throw an error
    if (!startDate && !endDate && !type && !status) {
      const error = new Error('Missing fields');
      error.httpStatus = 400;
      throw error;
    }

    // Get a connection to DB
    connection = await getDB();

    // Get data from rental request
    const [[request]] = await connection.query(
      `SELECT contract_start_date, contract_end_date, status, type
       FROM rentals 
       WHERE id = ?`,
      [idRental]
    );

    const newStartDate = startDate || request.contract_start_date;
    const newEndDate = endDate || request.contract_end_date;
    const newType = type || request.type;
    const newStatus = status || request.status;

    // If there are changes we collect them, otherwise we keep the database
    await connection.query(
      `UPDATE rentals
       SET status = ?, contract_start_date = ?, contract_end_date = ?, type = ?, modifiedAt = ?
       WHERE id = ?`,
      [newStatus, newStartDate, newEndDate, newType, new Date(), idRental]
    );

    res.send({
      status: 'ok',
      message: 'Rental modified successfully',
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = editRental;
