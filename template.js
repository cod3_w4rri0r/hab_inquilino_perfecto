
const getDB = require('../../database/getDB');


const template = async (req, res, next) => {
  let connection;

  try {
    connection = await getDB();

    res.send();

  } catch (error) {
    next(error);
    
  } finally {
    if (connection) connection.release();
  }
};

module.exports = template;
