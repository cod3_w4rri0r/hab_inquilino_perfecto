require('dotenv').config();

const mysql = require('mysql2/promise');

const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;

// Variable that will store a pool of connections
let pool;

// Function that returns a connection to the database.
const getDB = async () => {
  // If there is no connection...
  if (!pool) {
    // We create a pool of connections.
    pool = mysql.createPool({
      connectionLimit: 10,
      host: MYSQL_HOST,
      user: MYSQL_USER,
      password: MYSQL_PASSWORD,
      database: MYSQL_DATABASE,
      timezone: 'Z',
    });
  }

  // We return a free connection.
  return await pool.getConnection();
};

// We export the function.
module.exports = getDB;
