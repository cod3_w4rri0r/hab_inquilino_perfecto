const getDB = require('./getDB');
const { format } = require('date-fns');
const faker = require('faker/locale/es');
const bcrypt = require('bcrypt');
const { generateRandomString } = require('../helpers');

// Rounds for bcrypt encryption algorithm
const rounds = 10;
const adminPass = '123456';
const usersPass = '123456';

/**
 * Function to format dates to SQL DATETIME
 *
 * @param {Date} date
 * @returns Formatted SQL DATETIME date
 */
function formatDate(date) {
  return format(date, 'yyyy-MM-dd HH:mm:ss');
}

/**
 * Function to get an integer random number from "min" to "max" value
 *
 * @param {Number} min
 * @param {Number} max
 * @returns Random number within [min, max] interval
 */
function getRandom(min, max) {
  return Math.round(Math.random() * (max - min) + min);
}

/**
 * Function that fills Inquilino Perfecto's platform database with
 * appropriate test data to debug users and estates endpoints
 *
 * @param {Number} numUsers Number of users to be inserted in "users"
 * DB table
 * @param {Number} numEstates Number of users to be inserted in "estates"
 * DB table
 */
const testDataInsert = async (numUsers, numEstates) => {
  let connection;

  try {
    // Connection to DB
    connection = await getDB();

    // Users creation
    // ADMIN
    // Creates and encrypts admin user password
    const ADMIN_PASS = await bcrypt.hash(adminPass, rounds);

    // Insert Admin User
    await connection.query(`
        INSERT INTO users (email, password, name, active, role, createdAt)
        VALUES (
            "admin@gmail.com",
            "${ADMIN_PASS}",
            "Admin",
            true,
            "admin",
            "${formatDate(new Date())}"
        )
    `);

    // NORMAL USERS
    for (let i = 0; i < numUsers; i++) {
      // Faker data
      const email = faker.internet.email();
      const nombre = faker.name.findName();
      const password = await bcrypt.hash(usersPass, rounds);

      // User insertion
      await connection.query(`
        INSERT INTO users (email, password, name, active, createdAt)
        VALUES (
            "${email}",
            "${password}",
            "${nombre}",
            true,
            "${formatDate(new Date())}"
        )
      `);
    }

    // Info for devs
    console.log('Users created');

    // Estates creation
    // Cities and types of estates
    const cities = [
      'Barcelona',
      'Bilbao',
      'Málaga',
      'Madrid',
      'Las Palmas de Gran Canaria',
    ];
    const types = ['flat', 'chalet', 'studio'];

    for (let i = 0; i < cities.length; i++) {
      for (let j = 0; j < numEstates; j++) {
        // Data to be inserted into estates table
        const address = generateRandomString(20);
        const cp = getRandom(20000, 40000);
        const price = getRandom(2, 9) * 100 + Math.floor(getRandom(0, 2)) * 50;
        const type = types[getRandom(0, types.length - 1)];
        const num_bedrooms = getRandom(1, 5);
        const num_toilettes = getRandom(1, 3);
        const furnished = Math.floor(getRandom(0, 11)) % 2 ? 0 : 1;
        const area = Math.round(getRandom(25, 250));
        const balcony = Math.floor(getRandom(0, 11)) % 2 ? 0 : 1;
        const laundry = Math.floor(getRandom(0, 11)) % 2 ? 0 : 1;
        const parking = Math.floor(getRandom(0, 11)) % 2 ? 0 : 1;
        const storage_room = Math.floor(getRandom(0, 11)) % 2 ? 0 : 1;
        const pets = Math.floor(getRandom(0, 11)) % 2 ? 0 : 1;
        const estate_ref = generateRandomString(20);
        const idUser = getRandom(2, numUsers + 1);

        // Data insertion
        await connection.query(`
          INSERT INTO estates (location, address, cp, price, type,
            num_bedrooms, num_toilettes, furnished, area, 
            balcony, laundry, parking, 
            storage_room, pets, estate_ref, idUser, createdAt)
          VALUES (
              "${cities[i]}",
              "${address}",
              "${cp}",
              "${price}",
              "${type}",
              "${num_bedrooms}",
              "${num_toilettes}",
              "${furnished}",
              "${area}",
              "${balcony}",
              "${laundry}",
              "${parking}",
              "${storage_room}",
              "${pets}",
              "${estate_ref}",
              "${idUser}",
              "${formatDate(new Date())}"
          )
        `);
      }
    }
    // Info for devs
    console.log('Estates created');
  } catch (err) {
    console.error(err);
  } finally {
    if (connection) await connection.release();
  }
};

module.exports = testDataInsert;
