const getDB = require('./getDB');
const testDataInsert = require('./testDataDBFill');

async function initDB() {
  // Connection to DB variable
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Inquilino Perfecto's DB deletion, if it already existed
    await connection.query(`DROP DATABASE IF EXISTS inquilino_perfecto`);

    // Inquilino Perfecto's DB creation from scratch
    await connection.query(`CREATE DATABASE inquilino_perfecto`);
    await connection.query(`use inquilino_perfecto`);

    // Info for devs
    console.log('Database created');

    // Users ("users") table creation
    await connection.query(`
    CREATE TABLE users (
      id INT PRIMARY KEY AUTO_INCREMENT,
      email VARCHAR(100) UNIQUE NOT NULL,
      password VARCHAR(100) NOT NULL,
      name VARCHAR(50) NOT NULL,
      surname VARCHAR(100),
      
      dni_nie VARCHAR(9),
      avatar VARCHAR(100),
      bio VARCHAR(100),
      role ENUM("admin", "renter", "landlord", "hybrid") DEFAULT "renter",
      score DECIMAL(2, 1),
      
      active BOOLEAN DEFAULT false,
      deleted BOOLEAN DEFAULT false,
      
      registrationCode VARCHAR(100),
      recoverCode VARCHAR(100),
      
      createdAt DATETIME NOT NULL,
      modifiedAt DATETIME
      )
      `);

    // Log registry ("log_reg") table creation
    await connection.query(`
    CREATE TABLE log_reg (
      id INT PRIMARY KEY AUTO_INCREMENT,
      idUser INT NOT NULL,
      login_datetime DATETIME NOT NULL,
      logout_datetime DATETIME,
      diff_days INT,
      diff_hours INT,
      diff_minutes INT,
      diff_seconds INT,
      diff_millis INT,
      FOREIGN KEY (idUser) REFERENCES users(id)
      )
      `);

    // Building and houses ("estates") table creation
    await connection.query(`
    CREATE TABLE estates (
      id INT PRIMARY KEY AUTO_INCREMENT,
      location VARCHAR(100) NOT NULL,
      address VARCHAR(100) NOT NULL,
      cp INT NOT NULL,
      price INT NOT NULL,                
      type ENUM("flat", "chalet", "studio") DEFAULT "flat",
      num_bedrooms INT NOT NULL,
      num_toilettes INT NOT NULL,
      furnished BOOLEAN DEFAULT false,
      area INT NOT NULL,
      energy_certificate ENUM("A", "B", "C", "D", "E", "F", "G", "T") DEFAULT "T",
      balcony BOOLEAN DEFAULT false,
      laundry BOOLEAN DEFAULT false,
      parking BOOLEAN DEFAULT false,
      storage_room BOOLEAN DEFAULT false,
      pets BOOLEAN DEFAULT false,
      estate_ref VARCHAR (100),
      status ENUM("available", "rented", "hidden", "deleted") DEFAULT "available",
      score DECIMAL(2, 1),                
      idUser INT NOT NULL,
      FOREIGN KEY (idUser) REFERENCES users(id),
      createdAt DATETIME NOT NULL,
      modifiedAt DATETIME
      )
      `);

    // Houses photos ("photos") table creation
    await connection.query(`
    CREATE TABLE photos (
      id INT PRIMARY KEY AUTO_INCREMENT,
      name VARCHAR(100) NOT NULL,
      idEstate INT NOT NULL,
      FOREIGN KEY (idEstate) REFERENCES estates(id) ON DELETE CASCADE,
      createdAt DATETIME NOT NULL
      )
      `);

    // Rental requests table (only requests)
    await connection.query(`
    CREATE TABLE rentals_requests (
      id INT PRIMARY KEY AUTO_INCREMENT,              
      idRenter INT NOT NULL,
      idLandlord INT NOT NULL,
      idEstate INT NOT NULL,
      FOREIGN KEY (idRenter) REFERENCES users(id) ON DELETE CASCADE,
      FOREIGN KEY (idLandlord) REFERENCES users(id) ON DELETE CASCADE,
      FOREIGN KEY (idEstate) REFERENCES estates(id) ON DELETE CASCADE,
      
      resolution_date DATETIME,
      status ENUM("pending", "accepted", "rejected", "cancelled") NOT NULL,

      request_ref VARCHAR(10),

      createdAt DATETIME NOT NULL,
      modifiedAt DATETIME
      
      )
      `);

    // Rentals table (only contracts)
    await connection.query(`
      CREATE TABLE rentals (
        id INT PRIMARY KEY AUTO_INCREMENT,              
        idRenter INT NOT NULL,
        idLandlord INT NOT NULL,
        idEstate INT NOT NULL,
        FOREIGN KEY (idRenter) REFERENCES users(id) ON DELETE CASCADE,
        FOREIGN KEY (idLandlord) REFERENCES users(id) ON DELETE CASCADE,
        FOREIGN KEY (idEstate) REFERENCES estates(id) ON DELETE CASCADE,
        
        status ENUM("active", "cancelled", "extended", "finished") NOT NULL,
  
        contract_start_date DATETIME NOT NULL,
        contract_end_date DATETIME,
  
        type ENUM("contract-long", "contract-short") NOT NULL,
  
        requests_ref VARCHAR(10),
        contract_ref VARCHAR(10),

        createdAt DATETIME NOT NULL,
        modifiedAt DATETIME, 
  
        estate_rating_code VARCHAR(10),
        exp_date_estate_code DATETIME,
        use_date_estate_code DATETIME,

        renter_rating_code VARCHAR(10),
        exp_date_renter_code DATETIME,
        use_date_renter_code DATETIME,

        landlord_rating_code VARCHAR(10),
        exp_date_landlord_code DATETIME,
        use_date_landlord_code DATETIME
        )
        `);

    // Estate ratings ("estate_rentings") table creation
    await connection.query(`
    CREATE TABLE estates_ratings (
      id INT PRIMARY KEY AUTO_INCREMENT,
      rating_avg DECIMAL(2, 1) NOT NULL,
      reliability_ad DECIMAL(2, 1) NOT NULL,   
      quality_materials DECIMAL(2, 1) NOT NULL,
      luminosity DECIMAL(2, 1) NOT NULL,
      comments TEXT,
      idEstate INT NOT NULL,
      idUser INT NOT NULL,
      FOREIGN KEY (idUser) REFERENCES users(id) ON DELETE CASCADE,
      FOREIGN KEY (idEstate) REFERENCES estates(id) ON DELETE CASCADE,
      createdAt DATETIME NOT NULL,
      modifiedAt DATETIME
      )
      `);

    // User ratings ("users_rentings") table creation
    await connection.query(`
    CREATE TABLE users_ratings (
      id INT PRIMARY KEY AUTO_INCREMENT,
      rating DECIMAL(2, 1),
      comments TEXT,
      idAuthorUser INT NOT NULL,
      idRatedUser INT NOT NULL,
      idRental INT NOT NULL,
      FOREIGN KEY (idAuthorUser) REFERENCES users(id) ON DELETE CASCADE,
      FOREIGN KEY (idRatedUser) REFERENCES users(id) ON DELETE CASCADE,
      FOREIGN KEY (idRental) REFERENCES rentals(id) ON DELETE CASCADE,
      createdAt DATETIME NOT NULL,
      modifiedAt DATETIME
      )
    `);

    console.log('Tables created');

    await testDataInsert(10, 20);
  } catch (err) {
    console.error(err);
  } finally {
    // If there's a connection, release it
    if (connection) connection.release();

    // Close current process (recovers terminal prompt)
    process.exit();
  }
}

// Call function above
initDB();
