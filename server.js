require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const fileUpload = require('express-fileupload');
const cors = require('cors');

const app = express();

const { PORT } = process.env;

/**
 * #################
 * ## Middlewares ##
 * #################
 */
const {
  isAuth,
  userExists,
  canEditUser,
  canEditEstate,
  rentalExists,
  canEditRental,
  canEditEstateRating,
  canEditUserRating,
  estateRatingExists,
  userRatingExists,
  estateExists,
} = require('./middlewares');

/**
 * ############################
 * ## Users controllers ##
 * ############################
 */
const {
  newUser,
  validateUser,
  login,
  logout,
  getUser,
  editUser,
  editUserPass,
  editUserAvatar,
  recoverUserPass,
  resetUserPass,
  deleteUser,
  getUserEstates,
} = require('./controllers/users');

/**
 * #############################
 * ## Estates controllers ##
 * #############################
 */
const {
  newEstate,
  editEstate,
  deleteEstate,
  searchEstates,
  changeStatusEstates,
} = require('./controllers/estates');

/**
 * #########################
 * ## Rentals controllers ##
 * #########################
 */
const {
  newRentalRequest,
  deleteRentalRequest,
  newRental,
  editRental,
  requestResponse,
  listRenterRentals,
  listLandlordContracts,
} = require('./controllers/rentals');

/**
 * #########################
 * ## Ratings controllers ##
 * #########################
 */
const {
  genEstateRatingCode,
  genUserRatingCode,
  addEstatesRating,
  addUsersRatings,
  editEstatesRating,
  editUsersRating,
  getEstatesRating,
  getRatedUserRatings,
  getAuthorUserRatings,
  deleteUserRating,
  deleteEstateRating,
} = require('./controllers/ratings');

/**
 * ######################
 * ## Misc controllers ##
 * ######################
 */

const getLocations = require('./controllers/misc/getLocations');

// Middleware that gives us information about requests that arrive on server
app.use(morgan('dev'));

// Middleware that deserialize a body in "raw" format and makes it available
// in property "request.body" of incoming request object
app.use(express.json());

// Middleware that deserialize a body in "form-data" format and makes it available
// in property "request.body". If there's any file it will be available in property
// "request.files"
app.use(fileUpload());

// Middleware to provide CORS Policy headers
app.use(cors());

// Middleware to allow access to static files
app.use('/uploads', express.static('static/uploads'));

/**
 * ########################
 * ## Endpoints Users ##
 * ########################
 */

// Create a new user
app.post('/users', newUser);

// Validate a user
app.get('/users/validate/:registrationCode', validateUser);

// Log in a user
app.post('/users/login', login);

// Log out a user (registry)
app.put('/users/logout', isAuth, logout);

// Get information of a user
app.get('/users/:idUser', isAuth, userExists, getUser);

// Get estates of a user (only for landlords)
app.get(
  '/users/:idUser/estates/',
  isAuth,
  userExists,
  canEditUser,
  getUserEstates
);

// Edit a user (Bio)
app.put('/users/:idUser', isAuth, userExists, canEditUser, editUser);

// Edit a user's password
app.put(
  '/users/:idUser/password',
  isAuth,
  userExists,
  canEditUser,
  editUserPass
);

// Edit a user's avatar
app.put(
  '/users/:idUser/avatar',
  isAuth,
  userExists,
  canEditUser,
  editUserAvatar
);

// Send a password recovering code to a user email
app.put('/users/password/recover', recoverUserPass);

// Reset a user's password using a recovering code
app.put('/users/password/reset', resetUserPass);

// Anonymize (delete) a user
app.delete('/users/:idUser', isAuth, userExists, canEditUser, deleteUser);

/**
 * #######################
 * ## Endpoints Estates ##
 * #######################
 */

// Register a new estate
app.post('/estates', isAuth, newEstate);

// Edit an estate
app.put(
  '/estates/edit/:idEstate',
  isAuth,
  estateExists,
  canEditEstate,
  editEstate
);

// Change Status
app.put(
  '/estates/changeStatus/:idEstate',
  isAuth,
  estateExists,
  canEditEstate,
  changeStatusEstates
);

// Search estates (filter)
app.get('/estates/search', searchEstates);

// Delete estate (status)
app.delete(
  '/estates/delete/:idEstate',
  isAuth,
  estateExists,
  canEditEstate,
  deleteEstate
);

/**
 * #######################
 * ## Endpoints Rentals ##
 * #######################
 */

// Add a new rental request (to an estate)
app.post('/rentals/newRequest', isAuth, newRentalRequest);

// Delete a rental request by its author (Renter)
app.delete('/rentals/deleteRequest', isAuth, deleteRentalRequest);

// Rental application response (by estate's landlord)
app.post('/rentals/resolve', isAuth, requestResponse);

// Add a rental (by landlord only)
app.post('/rentals/newRental/:idRentalReq', isAuth, newRental);

// Modify a rental (by landlord only)
app.put('/rentals/:idRental', isAuth, canEditRental, editRental);

// Renter Rentals (List)
app.get('/rentals/renter', isAuth, listRenterRentals);

// Landlord Contracts (List)
app.get('/rentals/landlord', isAuth, listLandlordContracts);

/**
 * #######################
 * ## Endpoints Ratings ##
 * #######################
 */

// Request an authorization code to rate an estate
app.get(
  '/ratings/rate/estates/:idRental',
  isAuth,
  rentalExists,
  genEstateRatingCode
);

// Request an authorization code to rate a user
app.get(
  '/ratings/rate/users/:idRental',
  isAuth,
  rentalExists,
  genUserRatingCode
);

// Add a rating to an estate
app.post('/ratings/estates/:idRental', isAuth, rentalExists, addEstatesRating);

// Add a rating to a user
app.post('/ratings/users/:idRental', isAuth, rentalExists, addUsersRatings);

// Edit Users Rating
app.put(
  '/ratings/editUsersRating/:idRating',
  isAuth,
  userRatingExists,
  canEditUserRating,
  editUsersRating
);

// Edit Estates Rating
app.put(
  '/ratings/editEstatesRating/:idRating',
  isAuth,
  estateRatingExists,
  canEditEstateRating,
  editEstatesRating
);

// Get Estates Ratings
app.get('/ratings/getEstatesRating/:idEstate', isAuth, getEstatesRating);

// Get Rated User Ratings (what they say about me)
app.get(
  '/ratings/getRatedUserRatings/:idRatedUser',
  isAuth,
  getRatedUserRatings
);

// Get Author User Ratings (what I say about them)
app.get(
  '/ratings/getAuthorUserRatings/:idAuthorUser',
  isAuth,
  getAuthorUserRatings
);

// Delete User Rating
app.delete(
  '/ratings/deleteUserRating/:idRating',
  isAuth,
  userRatingExists,
  canEditUserRating,
  deleteUserRating
);

// Delete Estate Rating
app.delete(
  '/ratings/deleteEstateRating/:idRating',
  isAuth,
  estateRatingExists,
  canEditEstateRating,
  deleteEstateRating
);

/**
 * ####################
 * ## Endpoints Misc ##
 * ####################
 */

// Get Locations, for filter fileds suggestions
app.get('/misc/locations', getLocations);

/**
 * ##################################
 * ## Middleware Error & Not Found ##
 * ##################################
 */

// Error middleware
// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  console.error(error);
  res.status(error.httpStatus || 500).send({
    status: 'error',
    message: error.message,
  });
});

// "Not-found" middleware
app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    message: 'Not found',
  });
});

// Get server listening in a given port
app.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}`);
});
