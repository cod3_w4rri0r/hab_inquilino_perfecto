const crypto = require('crypto');
const sgMail = require('@sendgrid/mail');
const sharp = require('sharp');
const path = require('path');
const uuid = require('uuid');
const { ensureDir, unlink } = require('fs-extra');

// We import the environment variables.
const { SENDGRID_API_KEY, SENDGRID_FROM, UPLOADS_DIRECTORY } = process.env;

// We assign the API KEY to SendGrid.
sgMail.setApiKey(SENDGRID_API_KEY);

// We create the absolute path to the file upload directory.
const uploadsDir = path.join(__dirname, UPLOADS_DIRECTORY);

/**
 * ######################################
 * ## Generate an alphanumeric string  ##
 * ######################################
 */
function generateRandomString(length) {
  return crypto.randomBytes(length).toString('hex');
}

/**
 * #####################
 * ## Send an email   ##
 * #####################
 */
async function sendMail({ to, subject, body }) {
  try {
    // We prepare the message.
    const msg = {
      to,
      from: SENDGRID_FROM,
      subject,
      text: body,
      html: `
                <div>
                    <h1>${subject}</h1>
                    <p>${body}</p>
                </div>
            `,
    };

    // We send the message
    await sgMail.send(msg);
  } catch (error) {
    console.error(error);
    throw new Error('There was a problem sending the message');
  }
}

/**
 * #####################################
 * ## Save a photo on the server ##
 * #####################################
 */
async function savePhoto(image, type) {
  try {
    // We check that the image upload directory exists.
    await ensureDir(uploadsDir);

    // We convert the image into an object "Sharp".
    const sharpImage = sharp(image.data);

    // We access the metadata of the image to later check
    // the full width.
    const imageInfo = await sharpImage.metadata();

    // If the image type is 0 (avatar) we resize the image to 150x150.
    if (type === 0) {
      sharpImage.resize(150, 150);
    }

    // If the image is type 2 (input) and the width exceeds the maximum indicated
    // we resize the image.
    else if (type === 1 && imageInfo.width > 1000) {
      sharpImage.resize(1000);
    }

    // We generate a unique name for the image.
    const imageName = `${uuid.v4()}.jpg`;

    // We create the absolute path to the location where we want to save the image
    const imagePath = path.join(uploadsDir, imageName);

    // We save the image in the "uploads" directory.
    await sharpImage.toFile(imagePath);

    // We return the name of the image
    return imageName;
  } catch (error) {
    console.error(error);
    throw new Error('Error processing image');
  }
}

/**
 * ####################################
 * ## Delete a photo from the server ##
 * ####################################
 */
async function deletePhoto(photoName) {
  try {
    // We create the absolute path to the photo.
    const photoPath = path.join(uploadsDir, photoName);

    // We delete the photo from the disk.
    await unlink(photoPath);
  } catch (error) {
    console.error(error);
    throw new Error('Error removing image from server');
  }
}

/**
 * #####################
 * ## Validate schema ##
 * #####################
 */
async function validate(schema, data) {
  try {
    await schema.validateAsync(data);
  } catch (error) {
    error.httpStatus = 400;
    throw error;
  }
}

module.exports = {
  generateRandomString,
  sendMail,
  savePhoto,
  deletePhoto,
  validate,
};
