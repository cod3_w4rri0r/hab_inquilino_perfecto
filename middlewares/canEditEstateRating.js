const getDB = require('../database/getDB');

const canEditEstateRating = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the id of the rating
    const { idRating } = req.params;

    // We get the id of the user who rated
    const [rating] = await connection.query(
      `SELECT idUser FROM estates_ratings WHERE id = ?`,
      [idRating]
    );

    // If the user making the request is not the author of rating, we throw an error
    if (req.userAuth.id !== rating[0].idUser) {
      const error = new Error('You do not have enough permissions');
      error.httpStatus = 403;
      throw error;
    }

    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = canEditEstateRating;
