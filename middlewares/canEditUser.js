const getDB = require('../database/getDB');

const canEditUser = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the id of the user we want to edit
    const { idUser } = req.params;

    // We throw an error in case both ids do not match
    if (Number(idUser) !== req.userAuth.id) {
      const error = new Error('You do not have enough permissions');
      error.httpStatus = 403;
      throw error;
    }

    // We pass control to the next function.
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = canEditUser;
