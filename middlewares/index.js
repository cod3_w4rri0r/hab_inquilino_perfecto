const isAuth = require('./isAuth');
const userExists = require('./userExists');
const canEditUser = require('./canEditUser');
const rentalExists = require('./rentalExists');
const canEditUserRating = require('./canEditUserRating');
const canEditEstateRating = require('./canEditEstateRating');
const canEditEstate = require('./canEditEstate');
const canEditRental = require('./canEditRental');
const estateRatingExists = require('./estateRatingExists');
const userRatingExists = require('./userRatingExists');
const estateExists = require('./estateExists');

module.exports = {
  isAuth,
  userExists,
  canEditUser,
  canEditEstate,
  rentalExists,
  canEditRental,
  canEditUserRating,
  canEditEstateRating,
  estateRatingExists,
  userRatingExists,
  estateExists,
};
