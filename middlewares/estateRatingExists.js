const getDB = require('../database/getDB');

const estateRatingExists = async (req, res, next) => {
  let connection;

  // Get rating id
  const { idRating } = req.params;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Try to get estate rating from DB
    const [rating] = await connection.query(
      `SELECT id FROM estates_ratings WHERE id = ?`,
      [idRating]
    );

    // If rental doesn't exist, throw an error
    if (rating.length < 1) {
      console.log('Estate rating with id:', idRating, "doesn't exist");
      const error = new Error('Rating does not exist');
      error.httpStatus = 404;
      throw error;
    }

    // Pass control to next function
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = estateRatingExists;
