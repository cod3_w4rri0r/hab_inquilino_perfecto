const getDB = require('../database/getDB');

const jwt = require('jsonwebtoken');

const isAuth = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the authorization header (the token).
    const { authorization } = req.headers;

    // If there is no authorization header we throw an error
    if (!authorization) {
      const error = new Error('Authorization header missing');
      error.httpStatus = 401;
      throw error;
    }

    // Variable that will store the information of the token: the id and the role
    let tokenInfo;

    try {
      // We decrypt the token
      tokenInfo = jwt.verify(authorization, process.env.SECRET);
    } catch (_) {
      const error = new Error('The token is not valid');
      error.httpStatus = 401;
      throw error;
    }

    // We select the user with the id that comes in the token
    const [users] = await connection.query(
      `SELECT active, deleted FROM users WHERE id = ?`,
      [tokenInfo.id]
    );

    // If the user is not activated or if it is deleted, we throw an error.
    if (!users[0].active || users[0].deleted) {
      const error = new Error('The token is not valid');
      error.httpStatus = 401;
      throw error;
    }

    // We inject the token info into the "request" object: id, role
    req.userAuth = tokenInfo;

    // We pass control to the next function.
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = isAuth;
