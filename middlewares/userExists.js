const getDB = require('../database/getDB');

const userExists = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // We get the user id.
    const { idUser } = req.params;

    // We get the user.
    const [users] = await connection.query(
      `SELECT id FROM users WHERE id = ? AND deleted = false`,
      [idUser]
    );

    // If the user does not exist we throw an error.
    if (users.length < 1) {
      const error = new Error('Username does not exist');
      error.httpStatus = 404;
      throw error;
    }

    // We pass control to the next function.
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = userExists;
