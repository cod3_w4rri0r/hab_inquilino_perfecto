const getDB = require('../database/getDB');

const estateExists = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get estate id from params
    const { idEstate } = req.params;

    // Look for the estate in DB
    const [estate] = await connection.query(
      `SELECT idUser FROM estates WHERE id = ?`,
      [idEstate]
    );

    // If no estate is found, throw an error
    if (estate[0].length === 0) {
      console.log('No estate found');
      const error = new Error('ERROR: Estate not found');
      error.httpStatus = 404;
      throw error;
    }

    // We pass control to the next function.
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = estateExists;
