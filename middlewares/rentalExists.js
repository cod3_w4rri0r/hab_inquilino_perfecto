const getDB = require('../database/getDB');

const rentalExists = async (req, res, next) => {
  let connection;

  // Get rental id
  const { idRental } = req.params;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Try to get rental from DB
    const [rental] = await connection.query(
      `SELECT id FROM rentals WHERE id = ?`,
      [idRental]
    );

    // If rental doesn't exist, throw an error
    if (rental.length < 1) {
      console.log('Rental with id:', idRental, "doesn't exist");
      const error = new Error('Rental does not exist');
      error.httpStatus = 404;
      throw error;
    }

    // Pass control to next function
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = rentalExists;
