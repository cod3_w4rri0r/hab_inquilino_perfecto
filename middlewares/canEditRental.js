const getDB = require('../database/getDB');

const canEditRental = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get rental's id from params
    const { idRental } = req.params;

    // Get user id of rental's landlord
    const [rental] = await connection.query(
      `SELECT idLandlord FROM rentals WHERE id = ?`,
      [idRental]
    );

    // If user sending request is not rental's landlord, throw an error
    if (req.userAuth.id !== rental[0].idLandlord) {
      console.log('User is not landlord in this rental');
      const error = new Error('ERROR: You have not permission');
      error.httpStatus = 403;
      throw error;
    }

    // Pass control to next function
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = canEditRental;
