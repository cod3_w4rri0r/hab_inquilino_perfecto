const getDB = require('../database/getDB');

const canEditEstate = async (req, res, next) => {
  let connection;

  try {
    // Get a connection to DB
    connection = await getDB();

    // Get estate id from params
    const { idEstate } = req.params;

    // Get user id from estate owner
    const [estate] = await connection.query(
      `SELECT idUser FROM estates WHERE id = ?`,
      [idEstate]
    );

    // If user sending request is not estate owner, throw an error
    if (req.userAuth.id !== estate[0].idUser) {
      console.log('User is not estate owner');
      const error = new Error('ERROR: You have not permission');
      error.httpStatus = 403;
      throw error;
    }

    // Pass control to next function
    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
};

module.exports = canEditEstate;
